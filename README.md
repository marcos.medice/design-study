# Design study project

This project was created for the study of design patterns and other technologies.

## Design Patterns:

Each design pattern will have a simple implementation and a Main class with an executable main method to test the design pattern.

Study material -> YouTube Channel: Geekific -> Playlist: Design Patterns
- https://youtube.com/playlist?list=PLlsmxlJgn1HJpa28yHzkBmUY-Ty71ZUGc

## Messaging:

Implementing some messaging tools

- Apache Kafka

## Multithread:

- https://www.youtube.com/playlist?list=PLuYctAHjg89YNXAXhgUt6ogMyPphlTVQG

Implementing some multithread classes

- Thread
- Runnable
- Executors