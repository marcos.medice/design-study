package com.design.study.remoteprocedurecall;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.MalformedURLException;
import java.net.URL;


public class RPCClient {
    private static final String SERVER_URL = "http://127.0.0.1:8185";
    private static XmlRpcClient client;

    public RPCClient () {
        try {
            XmlRpcClientConfigImpl clientConfig = new XmlRpcClientConfigImpl();
            clientConfig.setServerURL(new URL(SERVER_URL));
            client = new XmlRpcClient();
            client.setConfig(clientConfig);
        } catch (MalformedURLException e) {
            System.out.println("Servidor RPC finalizado. Exceção: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        try {
            new RPCClient();
            Object[] params = new Object[]{};
            Boolean status = (Boolean) client.execute("Procedure.rpcStatus", params);
            System.out.println("Status: " + status);
            if (status) {
                params = new Object[]{"teste1", "teste2"};
                client.execute("Procedure.rpcLog", params);
            }
        } catch (Exception e) {
            System.out.println("Erro ao executar: " + e.getMessage());
        }
    }
}
