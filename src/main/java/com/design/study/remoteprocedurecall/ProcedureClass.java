package com.design.study.remoteprocedurecall;

public class ProcedureClass {
    public boolean rpcStatus() {
        System.out.println("Rpc Status");
        return true;
    }

    public boolean rpcLog(String param1, String param2) {
        System.out.println("Teste RPC!!! param1: " + param1 + " param2: " + param2);
        return true;
    }
}
