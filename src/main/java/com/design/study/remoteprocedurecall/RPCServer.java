package com.design.study.remoteprocedurecall;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.webserver.WebServer;

import java.io.IOException;

public class RPCServer {
    private RPCServer () {
        WebServer webServer = null;
        try {
            webServer = new WebServer(8185);
            XmlRpcServer server = webServer.getXmlRpcServer();
            PropertyHandlerMapping propertyHandlerMapping = new PropertyHandlerMapping();
            propertyHandlerMapping.addHandler("Procedure", ProcedureClass.class);
            server.setHandlerMapping(propertyHandlerMapping);
            webServer.start();
            System.out.println("Servidor RPC iniciado na porta 8185");
        } catch (XmlRpcException | IOException e) {
            webServer.shutdown();
            System.out.println("Servidor RPC finalizado. Exceção: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new RPCServer();
    }
}
