package com.design.study.messaging.kafka.singlepartition.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Collections;

public class Consumer {
    private static final String TOPIC_NAME = "topic.teste";

    public static void main(String[] args) {
        // try-with-resources
        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(KafkaConsumerConfig.properties())) {
            consumer.subscribe(Collections.singletonList(TOPIC_NAME));
            while (true) {
                ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(100));
                consumerRecords.forEach(consumerRecord -> {
                    System.out.println("Nova mensagem:");
                    System.out.println("Key: " + consumerRecord.key());
                    System.out.println("Value: " + consumerRecord.value());
                    System.out.println("Offset: " + consumerRecord.offset());
                    System.out.println("Partition: " + consumerRecord.partition());
                });
            }
        } catch (Exception e) {
            System.out.println("Nao foi possivel inicializar o consumer: " + e.getMessage());
        }
    }
}
