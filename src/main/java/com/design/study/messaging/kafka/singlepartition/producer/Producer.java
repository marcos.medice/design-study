package com.design.study.messaging.kafka.singlepartition.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.concurrent.ExecutionException;

public class Producer {
    private static final String TOPIC_NAME = "topic.teste";

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // try-with-resources
        try (KafkaProducer<String, String> producer = new KafkaProducer<>(KafkaProducerConfig.properties());) {
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>(TOPIC_NAME,
                    "teste-1", "TESTE MENSAGEM");
            producer.send(producerRecord, KafkaProducerConfig.producerCallback()).get();
        } catch (Exception e) {
            System.out.println("Nao foi possivel inicializar o producer: " + e.getMessage());
        }
    }
}
