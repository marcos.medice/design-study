package com.design.study.messaging.kafka.springboot;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    @KafkaListener(topics = "topic.teste2", groupId = "consumer-teste2")
    public void consumer(String message) {
        System.out.println("NOVA MENSAGEM: " + message);
    }
}
