package com.design.study.messaging.kafka.multiplepartition.producer;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class KafkaProducerConfig {

    /*
        Configuracao producer do kafka
            server
            serializador de chave
            serializador de valor
     */
    public static Properties properties() {
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return properties;
    }

    /*
        Configuracao do callback do producer
     */
    public static Callback producerCallback() {
        Callback callback = (data, error) -> {
            if (error != null) {
                error.printStackTrace();
                return;
            }
            System.out.println("Mensagem publicada:");
            System.out.println("Partition: " + data.partition());
            System.out.println("Offset: " + data.offset());
        };
        return callback;
    }
}
