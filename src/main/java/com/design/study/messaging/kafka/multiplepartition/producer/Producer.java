package com.design.study.messaging.kafka.multiplepartition.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.concurrent.ExecutionException;

public class Producer {
    private static final String TOPIC_NAME = "topic.teste2";

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // try-with-resources
        try (KafkaProducer<String, String> producer = new KafkaProducer<>(KafkaProducerConfig.properties());) {


            for (int i = 0; i<10; i++) {
                ProducerRecord<String, String> producerRecord = new ProducerRecord<>(TOPIC_NAME, "teste-" + i, "TESTE MENSAGEM " + i);
                producer.send(producerRecord, KafkaProducerConfig.producerCallback()).get();
            }
        } catch (Exception e) {
            System.out.println("Nao foi possivel inicializar o producer: " + e.getMessage());
        }
    }
}
