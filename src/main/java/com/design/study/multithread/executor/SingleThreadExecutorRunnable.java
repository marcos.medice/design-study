package com.design.study.multithread.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class SingleThreadExecutorRunnable {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = null;
        try {
            executorService = Executors.newSingleThreadExecutor();

            executorService.execute(new Task()); // Roda run do runnable
            executorService.execute(new Task()); // Roda run do runnable
            executorService.execute(new Task()); // Roda run do runnable

            Future<?> future = executorService.submit(new Task()); // Roda run do runnable mas retorna infos da execução
            System.out.println("SUBMIT FUTURE isDone: " + future.isDone());

            executorService.shutdown();
            executorService.awaitTermination(5, TimeUnit.SECONDS); // espera valor do timeout para executar e não dar shutdown antes de terminar a execução
            System.out.println("SUBMIT FUTURE isDone: " + future.isDone());
        } catch (Exception e) {
            throw e;
        } finally {
            if (executorService != null) {
                executorService.shutdownNow(); // termina o executor
            }
        }
    }

    public static class Task implements Runnable {
        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + ": RODANDO TASK");
        }
    }
}
