package com.design.study.multithread.executor;

import java.util.Random;
import java.util.concurrent.*;

public class SingleThreadExecutorCallable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = null;
        try {
            executorService = Executors.newSingleThreadExecutor();

            Future<String> future = executorService.submit(new Task()); // Roda run do runnable mas retorna infos da execução
            System.out.println("isDone: " + future.isDone());
            System.out.println("RETORNO: " + future.get()); // .get() espera a execução terminar para obter o retorno
//            System.out.println("RETORNO: " + future.get(1, TimeUnit.SECONDS)); // .get() com timeout
            System.out.println("isDone: " + future.isDone());
        } catch (Exception e) {
            throw e;
        } finally {
            if (executorService != null) {
                executorService.shutdownNow(); // termina o executor
            }
        }
    }

    public static class Task implements Callable<String> {
        @Override
        public String call() {
            int nextInt = new Random().nextInt(1000);
            return Thread.currentThread().getName() + ": RODANDO TASK " + nextInt;
        }
    }
}
