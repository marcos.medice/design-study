package com.design.study.multithread.executor;

import java.util.Random;
import java.util.concurrent.*;

/*
    Descomentar o bloco a ser testado:
     - CALLABLE
     - RUNNABLE
     - FIXED RATE
     - FIXED DELAY
 */
public class ScheduledExecutor {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);

        // CALLABLE
//        ScheduledFuture<String> future = executor.schedule(new TaskCallable(), 1, TimeUnit.SECONDS);
//        System.out.println(future.get());
//        executor.shutdown();

        // RUNNABLE
//        executor.schedule(new TaskRunnable(), 1, TimeUnit.SECONDS);
//        executor.shutdown();

        // FIXED RATE
//        executor.scheduleAtFixedRate(new TaskRunnable(), 0, 1, TimeUnit.SECONDS);

        // FIXED DELAY
//        executor.scheduleWithFixedDelay(new TaskRunnable(), 0, 1, TimeUnit.SECONDS);
    }

    public static class TaskCallable implements Callable<String> {
        @Override
        public String call() {
            int nextInt = new Random().nextInt(1000);
            return Thread.currentThread().getName() + ": RODANDO TASK CALLABLE " + nextInt;
        }
    }

    public static class TaskRunnable implements Runnable {
        @Override
        public void run() {
            int nextInt = new Random().nextInt(1000);
            System.out.println(Thread.currentThread().getName() + ": RODANDO TASK RUNNABLE " + nextInt);
        }
    }
}
