package com.design.study.multithread.executor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class MultiThreadExecutor {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executor = null;
        try {
            System.out.println("SUBMIT: ");
//            executor = Executors.newFixedThreadPool(2); // quantidade fixa de threads
            executor = Executors.newCachedThreadPool(); // não é necessário passar o tamanho da thread pool, reutiliza threads livres ou cria uma nova. Realizar tarefas pequenas
            Future<String> future1 = executor.submit(new Task());
            System.out.println(future1.get());
            Future<String> future2 = executor.submit(new Task());
            Future<String> future3 = executor.submit(new Task());
            System.out.println(future2.get());
            System.out.println(future3.get());

            System.out.println("#####################################");
            System.out.println("INVOKE ALL: ");

            List<Task> taskList = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                taskList.add(new Task());
            }
            List<Future<String>> futureList = executor.invokeAll(taskList);
            for (Future<String> future : futureList) {
                System.out.println(future.get());
            }


            System.out.println("#####################################");
            System.out.println("INVOKE ANY: ");

            String value = executor.invokeAny(taskList); // executa todas tarefas mas obtém o retorno somente de uma execução
            System.out.println(value);

            executor.shutdown();
        } catch (Exception e) {
            throw e;
        } finally {
            if (executor != null) {
                executor.shutdownNow();
            }
        }
    }

    public static class Task implements Callable<String> {
        @Override
        public String call() {
            int nextInt = new Random().nextInt(1000);
            return Thread.currentThread().getName() + ": RODANDO TASK " + nextInt;
        }
    }
}
