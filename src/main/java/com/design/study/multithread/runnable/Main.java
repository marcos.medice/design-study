package com.design.study.multithread.runnable;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i <= 5; i++) {
            MultithreadClass runnable = new MultithreadClass(i);
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }
}
