package com.design.study.multithread.runnable;

/*
    Implementar a interface Runnable e o método run
    Permite que a classe estenda outra classe (não somente Thread) e implementar outras interfaces
 */
public class MultithreadClass implements Runnable {
    private final int threadNumber;

    public MultithreadClass (int threadNumber) {
        this.threadNumber = threadNumber;
    }

    @Override
    public void run() {
        System.out.println("Thread " + Thread.currentThread().getId() + " Running");
        for (int i = 0; i < 5; i++) {
            System.out.println("THREAD " + Thread.currentThread().getId() + ": NUMERO -> " + i + " THREAD NUMBER: " + threadNumber);

//            if (threadNumber == 2 && i == 2) {
//                throw new RuntimeException();
//            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
