package com.design.study.multithread.tools.counter;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CountDownLatchEx1 {

    private static volatile int i = 0;
    private static CountDownLatch countDownLatch = new CountDownLatch(3);

    // alterar o valor de i a cada 3 execuções
    public static void main(String[] args) {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(4);

        Runnable r1 = () -> {
            int j = new Random(1000).nextInt();
            int x = i * j;
            System.out.println(i + "*" + j + " = " + x);
            countDownLatch.countDown();
        };

        Runnable r2 = () -> {
            await();
            i = new Random().nextInt(100);
        };

        Runnable r3 = () -> {
            await();
            countDownLatch = new CountDownLatch(3);
        };

        Runnable r4 = () -> {
            await();
            System.out.println("Fim");
        };

        executor.scheduleAtFixedRate(r1, 0, 1, TimeUnit.SECONDS);
        executor.scheduleWithFixedDelay(r2, 0, 1, TimeUnit.SECONDS);
        executor.scheduleWithFixedDelay(r3, 0, 1, TimeUnit.SECONDS);
        executor.scheduleWithFixedDelay(r4, 0, 1, TimeUnit.SECONDS);


//        while (true) {
//            await();
//            i = new Random().nextInt(100);
//            countDownLatch = new CountDownLatch(3); // Não reutilizável, necessário reiniciar
//        }
    }

    private static void await(){
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
