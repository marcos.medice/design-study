package com.design.study.multithread.tools.cyclicbarrier;

import java.util.concurrent.*;

public class CyclicBarrierEx2 {

    private static BlockingQueue<Double> result = new LinkedBlockingQueue<Double>();
    private static ExecutorService executor;
    private static Runnable r1;
    private static Runnable r2;
    private static Runnable r3;
    private static double finalResult = 0;

    // 100*12 + 2^8 + 20*20/12 =
    public static void main(String[] args) {

        Runnable summarization = () -> {
            System.out.println("SOMANDO");
            finalResult += result.poll();
            finalResult += result.poll();
            finalResult += result.poll();
            System.out.println("FIM SOMA. RESULTADO: " + finalResult);
            restart();
        };

        executor = Executors.newFixedThreadPool(3);

        CyclicBarrier cyclicBarrier = new CyclicBarrier(3, summarization); // qtd de threads para esperar

        r1 = () -> {
            System.out.println(Thread.currentThread().getName());
            result.add(100d*12d);
            await(cyclicBarrier);
            System.out.println(Thread.currentThread().getName());
            sleep();
        };

        r2 = () -> {
            System.out.println(Thread.currentThread().getName());
            result.add(Math.pow(2d, 8d));
            await(cyclicBarrier);
            System.out.println(Thread.currentThread().getName());
            sleep();
        };

        r3 = () -> {
            System.out.println(Thread.currentThread().getName());
            result.add(20d*20d/12d);
            await(cyclicBarrier);
            System.out.println(Thread.currentThread().getName());
            sleep();
        };

        restart();
//        executor.shutdown();
    }

    private static void await(CyclicBarrier cyclicBarrier) {
        try {
            cyclicBarrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
    }

    private static void restart() {
        executor.submit(r1);
        executor.submit(r2);
        executor.submit(r3);
    }

    private static void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
