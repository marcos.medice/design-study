package com.design.study.multithread.tools.semaphore;

import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class SemaphoreEx2 {
    private static final Semaphore SEMAPHORE = new Semaphore(3);
    private static final AtomicInteger QUANTITY = new AtomicInteger(0);

    public static void main(String[] args) {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(501);

        Runnable r1 = () -> {
            int user = new Random().nextInt(10000);

            boolean access = false;
            QUANTITY.incrementAndGet();
            while (!access) {
                access = tryAcquire(); // permite somente 3 estarem executando além deste ponto
            }
            QUANTITY.decrementAndGet();

            System.out.println("User: " + user + " THREAD: " + Thread.currentThread().getName());
            sleep(); // simular execução de uma tarefa
            SEMAPHORE.release(); // liberar uma vaga para outra thread executar
        };

        Runnable r2 = () -> {
            System.out.println(QUANTITY.get());
        };

        for (int i = 0; i < 500; i++) {
            executor.execute(r1);
        }
        executor.scheduleWithFixedDelay(r2, 0, 100, TimeUnit.MILLISECONDS);
    }

    private static boolean tryAcquire() {
        try {
            return SEMAPHORE.tryAcquire(1, TimeUnit.SECONDS); // quanto tempo esperar
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
            return false;
        }
    }

    private static void sleep() {
        // 1 a 3 segundos
        try {
            int time = new Random().nextInt(3);
            time++;
            Thread.sleep(1000 * time);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
}
