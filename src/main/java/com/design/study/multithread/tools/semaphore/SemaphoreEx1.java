package com.design.study.multithread.tools.semaphore;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreEx1 {
    private static final Semaphore SEMAPHORE = new Semaphore(3);

    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();

        Runnable r1 = () -> {
            int user = new Random().nextInt(10000);
            acquire(); // permite somente 3 estarem executando além deste ponto
            System.out.println("User: " + user + " THREAD: " + Thread.currentThread().getName());
            sleep(); // simular execução de uma tarefa
            SEMAPHORE.release(); // liberar uma vaga para outra thread executar
        };

        for (int i = 0; i < 500; i++) {
            executor.execute(r1);
        }
        executor.shutdown();
    }

    private static void acquire() {
        try {
            SEMAPHORE.acquire();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    private static void sleep() {
        // 1 a 3 segundos
        try {
            int time = new Random().nextInt(3);
            time++;
            Thread.sleep(1000 * time);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
}
