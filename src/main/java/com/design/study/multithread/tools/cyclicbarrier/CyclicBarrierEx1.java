package com.design.study.multithread.tools.cyclicbarrier;

import java.util.concurrent.*;

public class CyclicBarrierEx1 {

    private static BlockingQueue<Double> result = new LinkedBlockingQueue<Double>();

    // 100*12 + 2^8 + 20*20/12 =
    public static void main(String[] args) {

        Runnable finalOperation = () -> {
            System.out.println("SOMANDO");
            Double finalResult = 0d;
            finalResult += result.poll();
            finalResult += result.poll();
            finalResult += result.poll();
            System.out.println("FIM SOMA. RESULTADO: " + finalResult);
        };

        CyclicBarrier cyclicBarrier = new CyclicBarrier(3, finalOperation); // qtd de threads para esperar
        ExecutorService executor = Executors.newFixedThreadPool(3);

        Runnable r1 = () -> {
            System.out.println(Thread.currentThread().getName());
            result.add(100d*12d);
            await(cyclicBarrier);
            System.out.println(Thread.currentThread().getName());
        };

        Runnable r2 = () -> {
            System.out.println(Thread.currentThread().getName());
            result.add(Math.pow(2d, 8d));
            await(cyclicBarrier);
            System.out.println(Thread.currentThread().getName());
        };

        Runnable r3 = () -> {
            System.out.println(Thread.currentThread().getName());
            result.add(20d*20d/12d);
            await(cyclicBarrier);
            System.out.println(Thread.currentThread().getName());
        };


        executor.submit(r1);
        executor.submit(r2);
        executor.submit(r3);

        executor.shutdown();
    }

    private static void await(CyclicBarrier cyclicBarrier) {
        try {
            cyclicBarrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
    }
}
