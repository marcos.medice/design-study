package com.design.study.multithread.tools.exchanger;

import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExchangerEx1 {

    private static Exchanger<String> EXCHANGER = new Exchanger<>();

    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();

        Runnable r1 = () -> {
            System.out.println(Thread.currentThread().getName() + " ENVIANDO R1");
            String message = "R1";
            String response = exchange(message);
            System.out.println(Thread.currentThread().getName() + " RECEBENDO " + response);
        };

        Runnable r2 = () -> {
            System.out.println(Thread.currentThread().getName() + " ENVIANDO R2");
            String message = "R2";
            String response = exchange(message);
            System.out.println(Thread.currentThread().getName() + " RECEBENDO " + response);
        };

        executor.execute(r1);
        executor.execute(r2);
        executor.shutdown();
    }

    private static String exchange(String message) {
        try {
            return EXCHANGER.exchange(message);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return e.getMessage();
        }
    }
}
