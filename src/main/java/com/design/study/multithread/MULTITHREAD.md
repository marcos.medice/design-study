# Multithread:

Classes multithread podem ser obtidas estendendo ou implementando:

- **Thread** (abstract class)
- **Runnable** (interface)
- **Executor + Runnable/Callable**

## Definições:

- ***start***: inicia execução da thread
- ***run***: executa na mesma thread (1 por vez)
- ***syncronized***: faz com que um bloco/trecho de código seja executado de forma "sequencial". Uma thread só executa assim que não houver outra executando. Utilizado para evitar problemas de concorrência.
- ***sleep***: pausa a thread por um período de tempo
- ***join***: faz com que seja necessário terminar a execução da thread para prosseguir/executar outras threads
- ***yield***: forma da thread informar o processador que não há trabalho a ser executado. Fornece tempo de processamento a outra thread.
- ***interrupt***: interrupção da thread
- ***submit (executor)***: retorna future
- ***execute (executor)***: sem retorno

## Paralelismo com estrutura de dados:

Sincronizar acesso a coleções

### Coleções sincronizadas:

java.util.collections

- ***Collections.syncronizedList***
- ***Collections.syncronizedMap***
- ***Collections.syncronizedSet***
- ***Collections.syncronizedCollection***

### Coleções thread-safe:

Não performático em operações inserir/remover, porque cria uma cópia sempre que executa as operações.

java.util.concurrent

- ***CopyOnWriteArrayList***
- ***CopyOnWriteArraySet***
- ***ConcurrentHashMap***
- ***LinkedBlockingQueue***
- ***LinkedBlockingDeque*** (double ended queue)

## Classes atômicas/operações atômicas:

Classes com operações que evitam problemas de concorrência em threads

java.util.concurrent

- ***AtomicInteger***
- ***AtomicLong***
- ***AtomicBoolean***
- ***AtomicReference***

## Volatile

faz com que as threads leiam o valor mais recente/atualizado de uma variável, ao invés da leitura de um cache local de processamento

opta pelo valor da RAM ao invés do cache do processador

## Classes auxiliares:

### CyclicBarrier

Usado quando é necessário que threads esperem por outras threads em um ponto

- ***await***: ponto onde as threads vão esperar pelas X threads para poder continuar

### CountDownLatch

Contador de execuções com execução final/sumarização

- ***countDown***: contagem do contador
- ***await***: ponto onde a thread espera o contador zerar para poder executar a partir dalí

### Semaphore

Limitar a quantidade de threads a executar um trecho simultaneamente

- ***acquire***: ponto onde só pode ter X threads executando a partir deste ponto
- ***release***: libera para outra thread executar
- ***tryAcquire***: tenta acessar, pode setar quanto tempo a thread vai esperar para ser liberada para executar

### Lock

Evitar que threads diferentes consigam acessar/modificar recursos ao mesmo tempo. Trava a execução a partir de um ponto e somente libera ao usar o ***unlock***. Vantagem sobre o syncronized de poder ser chamado em outras classes do código.

- ***lock***: trava um trecho de código para que outras threads não executem
- ***unlock***: libera o trecho do código
- ***tryLock***: tenta acessar o trecho do código com trava, pode ser configurado com tempo
- ***writeLock***: quando há alteração de algum recurso. Bloqueia outras threads
- ***readLock***: somente leitura do recurso. Não bloqueia outra thread de acessar

### SynchronousQueue

Trocar informações entre threads, especialmente entre 2 threads. A execução da inserção/obtenção só continua assim que os 2 são executados. Necessário que tenha uma thread colocando na fila e outra extraindo

- ***put***: adiciona obj na fila
- ***take***: obtém obj da fila
- ***pool***: obtém da fila, porém pode setar tempo máximo tentando obter
- ***offer***: tenta adicionar à fila, pode setar tempo máximo tentando adicionar

### Exchanger

Troca de informações entre duas threads.

- ***exchange***: faz a troca de infos entre as duas threads.

## Conceitos:

- ***Race Condition***: situação onde threads podem ter problemas com recursos concorridos, podendo ocorrer deadlock
- ***Deadlock***: impasse onde não há saída
- ***Critical Region***: região onde ocorre concorrência
- ***Mutex (Mutual Exclusion)***: proteção dos recursos que sofrem concorrência, não permite que mais de uma thread faça alteração no recurso