package com.design.study.multithread.thread;

/*
    Estender a classe Thread e sobrescrever os métodos
    Impede de estender outras classes além de Thread
 */
public class MultithreadClass extends Thread {

    private final int threadNumber;

    public MultithreadClass (int threadNumber) {
        this.threadNumber = threadNumber;
    }

    @Override
    public void run() {
        System.out.println("Thread " + Thread.currentThread().getId() + " Running");
        for (int i = 0; i < 5; i++) {
            System.out.println("THREAD " + Thread.currentThread().getId() + ": NUMERO -> " + i + " THREAD NUMBER: " + threadNumber);

//            if (threadNumber == 2 && i == 2) {
//                throw new RuntimeException();
//            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
