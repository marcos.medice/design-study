package com.design.study.multithread.thread;

/*
    Run: sequential
    Start: parallel
 */
public class Main {
    public static void main(String[] args) {
        for (int i = 0; i <= 5; i++) {
            MultithreadClass thread = new MultithreadClass(i);
            thread.start();
        }
    }
}
