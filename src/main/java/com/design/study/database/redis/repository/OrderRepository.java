package com.design.study.database.redis.repository;

import com.design.study.database.redis.entity.Order;
import com.redis.om.spring.repository.RedisDocumentRepository;

public interface OrderRepository extends RedisDocumentRepository<Order, Long> {
}
