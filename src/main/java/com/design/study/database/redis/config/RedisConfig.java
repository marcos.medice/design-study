package com.design.study.database.redis.config;

import com.design.study.database.redis.entity.Advertiser;
import com.design.study.database.redis.entity.Order;
import com.design.study.database.redis.repository.OrderRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.geo.Point;

import java.math.BigDecimal;

@Configuration
public class RedisConfig {

    @Bean
    CommandLineRunner loadTestData(OrderRepository repo) {
        return args -> {
            repo.deleteAll();

            String url = "http://test.com.br/";

            Advertiser advertiser = Advertiser.of("Test", url, new Point(153.616667, -28.716667));

            Order order = Order.of(advertiser, "Order Test", BigDecimal.valueOf(156.25), 11);

            repo.save(order);
        };
    }
}
