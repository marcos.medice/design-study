package com.design.study.database.redis.entity;

import com.redis.om.spring.annotations.Document;
import com.redis.om.spring.annotations.Indexed;
import com.redis.om.spring.annotations.Searchable;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
public class Advertiser {
    @Id
    @Indexed
    private Long id;

    // Indexed - match exato de texto
    @Indexed
    @NonNull
    private String name;

    // Indexed - match full-text
    @Searchable
    @NonNull
    private String url;

    // Indexed - Filtro Geo
    @Indexed
    @NonNull
    private Point local;

}
