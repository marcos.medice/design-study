package com.design.study.database.redis.entity;

import com.redis.om.spring.annotations.Document;
import com.redis.om.spring.annotations.Indexed;
import lombok.*;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(staticName = "of")
public class Order {
    @Id
    @Indexed
    private Long id;

    // Nested
    @Indexed
    @NonNull
    private Advertiser advertiser;

    @Indexed
    @NonNull
    private String name;

    @Indexed
    @NonNull
    private BigDecimal value;

    // Indexed - match numérico
    @Indexed
    @NonNull
    private Integer quantity;
}
