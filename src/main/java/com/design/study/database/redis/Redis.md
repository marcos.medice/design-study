# REDIS

## Annotations

Application:
- **@EnableRedisDocumentRepositories**: habilita repositorios redis

Entity:
- **@Document**: marca o objeto como entidade Redis para ser persistido como documento JSON pelo tipo apropriado de repositorio
- **@Indexed**: adiciona índice ao campo da entidade que será adicionada no schema de indices
- **@Searchable**: adiciona busca Full-Text ao índice do campo da entidade

## Tipos de consultas RediSearch

- **Text (full-text searches)**: texto
- **Tag (exact-match searches)**: tags
- **Numeric (range queries)**: valor numerico
- **Geo (geographic range queries)**: filtro geo
- **Vector (vector similarity queries)**: vetor

## Repositorio

RedisDocumentRepository <--- PagingAndSortingRepository <--- CrudRepository

