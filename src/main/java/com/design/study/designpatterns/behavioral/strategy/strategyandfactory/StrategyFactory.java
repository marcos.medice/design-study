package com.design.study.designpatterns.behavioral.strategy.strategyandfactory;

import java.util.HashMap;
import java.util.Map;

public class StrategyFactory {
    private static Map<String, Class> strategies = new HashMap<String, Class>();

    private static void loadMap() {
        strategies.put("A", StrategyA.class);
        strategies.put("B", StrategyB.class);
    }

    // IMPLEMENTACAO COM DESIGN PATTERN FACTORY

    public static IStrategy<String> getStrategy(String strategy) throws Exception {
        if (strategies.isEmpty()) {
            loadMap();
        }
        Class klass = strategies.get(strategy);
        return (IStrategy<String>) klass.getDeclaredConstructor().newInstance();
    }
}
