package com.design.study.designpatterns.behavioral.command;

public interface Receiver {
    void switchOn();
    void switchOff();
    void changeChannel();
    void mute();
}
