package com.design.study.designpatterns.behavioral.strategy.strategyandfactory;

public class Main {

    public static void main(String[] args) throws Exception {
        IStrategy<String> strategyA = StrategyFactory.getStrategy("A");
        IStrategy<String> strategyB = StrategyFactory.getStrategy("B");

        System.out.println("CALLING STRATEGY A RUN");
        System.out.println(strategyA.run());
        System.out.println("CALLING STRATEGY B RUN");
        System.out.println(strategyB.run());
    }
}
