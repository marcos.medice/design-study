package com.design.study.designpatterns.behavioral.command;

import com.design.study.designpatterns.behavioral.command.commands.Command;

public class Invoker {
    private Command command;

    public Invoker(Command command){
        this.command = command;
    }

    public void execute(){
        this.command.execute();
    }
}
