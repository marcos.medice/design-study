package com.design.study.designpatterns.behavioral.templatemethod;

import java.util.Random;

public class ObjectA extends BaseObjectMethod {
    @Override
    int method1() {
        System.out.println("ObjectA: Running method1...");
        return new Random().nextInt(10);
    }

    @Override
    void method2(int archives) {
        System.out.println("ObjectA: Running method2... Archives: " + archives);
    }

    @Override
    void method3() {
        System.out.println("ObjectA: Running method3...");
    }

    @Override
    void method4() {
        System.out.println("ObjectA: Running method4...");
    }
}
