package com.design.study.designpatterns.behavioral.templatemethod;

public class ObjectB extends BaseObjectMethod {
    @Override
    int method1() {
        System.out.println("ObjectB: Running method1...");
        return 4;
    }

    @Override
    void method2(int archives) {
        System.out.println("ObjectB: Running method2... Archives: " + archives);
    }

    @Override
    void method3() {
        System.out.println("ObjectB: Running method3...");
    }

    @Override
    void method4() {
        System.out.println("ObjectB: Running method4...");
    }
}
