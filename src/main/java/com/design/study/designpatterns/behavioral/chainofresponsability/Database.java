package com.design.study.designpatterns.behavioral.chainofresponsability;

import java.util.HashMap;
import java.util.Map;

public class Database {
    private final Map<String, String> users;

    public Database() {
        users = new HashMap<>();
        users.put("user1", "user1");
        users.put("user2", "user2");
    }

    public boolean isValidUser(String username) {
        return users.containsKey(username);
    }

    public boolean isValidPassword(String username, String password) {
        return users.get(username).equals(password);
    }
}
