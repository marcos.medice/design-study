package com.design.study.designpatterns.behavioral.strategy.strategywithenum;

public class StrategyB implements IStrategy<String> {
    @Override
    public String run() {
        return "Running strategy B";
    }
}
