package com.design.study.designpatterns.behavioral.strategy.strategywithenum;

public class StrategyA implements IStrategy<String> {
    @Override
    public String run() {
        return "Running strategy A";
    }
}
