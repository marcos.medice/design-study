package com.design.study.designpatterns.behavioral.state;

public class Phone {
    protected State state;

    public Phone() {
        state = new OffState(this);
    }

    public void setState(State state) {
        this.state = state;
    }

    public String lock() {
        return "Locking phone - turning off the screen";
    }

    public String home() {
        return "Home screen";
    }

    public String unlock() {
        return "Unlocking phone - home screen";
    }

    public String turnOn() {
        return "Turn on screen - phone locked";
    }

}
