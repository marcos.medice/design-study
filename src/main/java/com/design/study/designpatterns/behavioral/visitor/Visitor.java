package com.design.study.designpatterns.behavioral.visitor;

public interface Visitor {
    void visit(Client client);
}
