package com.design.study.designpatterns.behavioral.mediator;

public abstract class Object {
    protected String name;
    protected Mediator mediator;

    public Object(String name, Mediator mediator) {
        this.name = name;
        this.mediator = mediator;
    }

    public abstract void sendMessage(String message);
    public abstract void receiveMessage(String message);
}