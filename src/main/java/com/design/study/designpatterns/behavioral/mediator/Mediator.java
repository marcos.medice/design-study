package com.design.study.designpatterns.behavioral.mediator;

public interface Mediator {
    void sendMessage(String message, Object object);
    void addObject(Object object);
}
