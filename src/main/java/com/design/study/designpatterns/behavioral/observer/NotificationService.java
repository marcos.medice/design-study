package com.design.study.designpatterns.behavioral.observer;

import java.util.*;

public class NotificationService {
    private final Map<Event, Set<EventListener>> listeners;

    public NotificationService() {
        listeners = new HashMap<>();
        Arrays.stream(Event.values()).forEach(event -> {
            listeners.put(event, new HashSet<>());
        });
    }

    public void subscribe(Event sensor, EventListener listener) {
        listeners.get(sensor).add(listener);
    }

    public void subscribe(Event sensor, List<EventListener> listeners) {
        listeners.forEach(listener -> {
            this.listeners.get(sensor).add(listener);
        });
    }

    public void unsubscribe(Event sensor, EventListener listener) {
        listeners.get(sensor).remove(listener);
    }

    public void notify(Event sensor) {
        listeners.get(sensor).forEach(listener -> {
            listener.update(sensor);
        });
    }
}
