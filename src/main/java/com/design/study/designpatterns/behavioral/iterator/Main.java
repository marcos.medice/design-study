package com.design.study.designpatterns.behavioral.iterator;

import com.design.study.designpatterns.behavioral.iterator.tree.BinarySearchTree;
import com.design.study.designpatterns.behavioral.iterator.tree.Tree;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static Vertex<Integer> v0 = new Vertex<>(0);
    public static Vertex<Integer> v1 = new Vertex<>(1);
    public static Vertex<Integer> v2 = new Vertex<>(2);
    public static Vertex<Integer> v3 = new Vertex<>(3);
    public static Vertex<Integer> v4 = new Vertex<>(4);
    public static Vertex<Integer> v5 = new Vertex<>(5);
    public static Vertex<Integer> v6 = new Vertex<>(6);
    public static Vertex<Integer> v7 = new Vertex<>(7);

    public static void main(String[] args) {
        setNeighbours();

        Iterator<Integer> depthFirstIterator = new DepthFirstIterator<>(v0);
        while (depthFirstIterator.hasNext()) {
            System.out.println(depthFirstIterator.getNext());
        }
    }

    /*
        TODO: CORRIGIR
     */
//    public static void main2(String[] args) {
//        setNeighbours();
//
//        Tree<Integer> depthFirstIterator = new BinarySearchTree<>().createDFSIterator(v0);
//    }

    public static void setNeighbours() {
        v0.setNeighbours(Arrays.asList(v1, v5, v6, v7));
        v1.setNeighbours(Arrays.asList(v2, v3, v6));
        v2.setNeighbours(Arrays.asList(v1, v4));
        v3.setNeighbours(List.of(v1));
        v4.setNeighbours(Arrays.asList(v2, v5, v7));
        v5.setNeighbours(Arrays.asList(v0, v4));
        v6.setNeighbours(Arrays.asList(v0, v1));
        v7.setNeighbours(Arrays.asList(v0, v4));
    }
}
