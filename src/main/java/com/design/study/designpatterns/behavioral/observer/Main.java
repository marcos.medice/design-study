package com.design.study.designpatterns.behavioral.observer;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        EventListener clp1 = new CLPEventListener("CLP01");
        EventListener clp2 = new CLPEventListener("CLP02");
        EventListener clp3 = new CLPEventListener("CLP03");
        EventListener emb1 = new EmbeededEventListener("EMB01");
        EventListener emb2 = new EmbeededEventListener("EMB02");
        EventListener emb3 = new EmbeededEventListener("EMB03");

        List<EventListener> listenerList = Arrays.asList(clp1, clp2, emb1, emb2);

        NotificationService notificationService = new NotificationService();
        notificationService.subscribe(Event.SENSOR_1, listenerList);
        notificationService.subscribe(Event.SENSOR_2, listenerList);
        notificationService.subscribe(Event.SENSOR_2, clp3);
        notificationService.subscribe(Event.SENSOR_2, emb3);

        System.out.println("ENVIANDO EVENTO SENSOR 1");
        notificationService.notify(Event.SENSOR_1);
        System.out.println("------------------------------------------");
        System.out.println("ENVIANDO EVENTO SENSOR 2");
        notificationService.notify(Event.SENSOR_2);
    }
}
