package com.design.study.designpatterns.behavioral.command.commands;

public interface Command {
    void execute();
}
