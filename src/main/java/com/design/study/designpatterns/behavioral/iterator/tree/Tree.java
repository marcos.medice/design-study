package com.design.study.designpatterns.behavioral.iterator.tree;

import com.design.study.designpatterns.behavioral.iterator.Iterator;
import com.design.study.designpatterns.behavioral.iterator.Vertex;

public interface Tree<T> {
    Iterator<T> createBFSIterator(Vertex<T> startVertex);
    Iterator<T> createDFSIterator(Vertex<T> startVertex);
}
