package com.design.study.designpatterns.behavioral.templatemethod;

public abstract class BaseObjectMethod {
    public void run() {
        int archives = method1();
        method2(archives);
        method3();
        method4();
        method5();
    }

    abstract int method1();
    abstract void method2(int archives);
    abstract void method3();
    abstract void method4();

    protected void method5() {
        System.out.println("Running method5...");
    }
}
