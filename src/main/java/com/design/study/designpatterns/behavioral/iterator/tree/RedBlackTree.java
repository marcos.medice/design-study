package com.design.study.designpatterns.behavioral.iterator.tree;

import com.design.study.designpatterns.behavioral.iterator.BreadthFirstIterator;
import com.design.study.designpatterns.behavioral.iterator.DepthFirstIterator;
import com.design.study.designpatterns.behavioral.iterator.Iterator;
import com.design.study.designpatterns.behavioral.iterator.Vertex;

public class RedBlackTree<T> implements Tree<T> {
    @Override
    public Iterator<T> createBFSIterator(Vertex<T> startVertex) {
        return new BreadthFirstIterator<T>(startVertex);
    }

    @Override
    public Iterator<T> createDFSIterator(Vertex<T> startVertex) {
        return new DepthFirstIterator<T>(startVertex);
    }
}
