package com.design.study.designpatterns.behavioral.strategy.strategyandfactory;

public interface IStrategy<R> {

    public R run();
}
