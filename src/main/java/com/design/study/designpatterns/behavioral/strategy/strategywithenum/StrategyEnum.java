package com.design.study.designpatterns.behavioral.strategy.strategywithenum;

import java.util.Arrays;

public enum StrategyEnum {

    STRATEGY_A("A", StrategyA.class),
    STRATEGY_B("B", StrategyB.class);

    private String strategyId;
    private Class strategy;

    private StrategyEnum(String strategyId, Class strategy) {
        this.strategyId = strategyId;
        this.strategy = strategy;
    }

    public IStrategy<String> getStrategyClass() throws Exception {
        return (IStrategy<String>) strategy.getDeclaredConstructor().newInstance();
    }

    public static StrategyEnum getById(String strategyId) {
        return Arrays.stream(StrategyEnum.values()).filter(item -> item.strategyId == strategyId).findFirst().orElse(null);
    }
}
