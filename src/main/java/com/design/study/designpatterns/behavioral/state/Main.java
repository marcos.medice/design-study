package com.design.study.designpatterns.behavioral.state;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Phone phone = new Phone();
        JButton home = new JButton("Home");
        home.addActionListener(e -> System.out.println(phone.state.onHome()));
        JButton onOff = new JButton("On/Off");
        onOff.addActionListener(e -> System.out.println(phone.state.onOffOn()));

        JFrame frame = new JFrame();
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.add(home);
        frame.add(onOff);
        frame.getRootPane().setDefaultButton(onOff);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(450,450);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
