package com.design.study.designpatterns.behavioral.visitor;

import java.util.Arrays;
import java.util.List;

/*
    TODO: CONFERIR
 */

public class Main {
    public static void main(String[] args) {
        Client c1 = new ClientA("A1");
        Client c2 = new ClientA("A2");
        Client c3 = new ClientB("B1");
        Client c4 = new ClientB("B2");
        Client c5 = new ClientC("C1");
        Client c6 = new ClientC("C2");

        List<Client> clientList = Arrays.asList(c1, c2, c3, c4, c5, c6);

        Visitor visitor = new VisitorA();
        clientList.forEach(client -> {
            client.run(visitor);
        });
    }
}
