package com.design.study.designpatterns.behavioral.visitor;

public class ClientB extends Client {
    public ClientB(String name) {
        super(name);
    }

    @Override
    public void run(Visitor visitor) {
        visitor.visit(this);
    }
}
