package com.design.study.designpatterns.behavioral.observer;

public class EmbeededEventListener implements EventListener {
    private final String embeededId;

    public EmbeededEventListener(String embeededId) {
        this.embeededId = embeededId;
    }

    @Override
    public void update(Event eventSensor) {
        System.out.println("Embarcado " + embeededId + " evento recebido -> " + eventSensor);
    }
}
