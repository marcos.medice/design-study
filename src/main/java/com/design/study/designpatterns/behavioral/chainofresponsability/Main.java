package com.design.study.designpatterns.behavioral.chainofresponsability;

public class Main {
    public static void main(String[] args) {
        Database database = new Database();

        Handler userExistsHandler = new UserExistsHandler(database);
        Handler validPasswordHandler = new ValidPasswordHandler(database);
        Handler roleCheckerHandler = new RoleCheckerHandler();
        userExistsHandler.setNextHandler(validPasswordHandler).setNextHandler(roleCheckerHandler);

        AuthService authService = new AuthService(userExistsHandler);
        System.out.println("TENTANDO LOGIN: user1 user1");
        authService.logIn("user1", "user1");
        System.out.println("TENTANDO LOGIN: user2 user2");
        authService.logIn("user2", "user2");
        System.out.println("TENTANDO LOGIN: user1 aaa");
        authService.logIn("user1", "aaa");
        System.out.println("TENTANDO LOGIN: user12 user1");
        authService.logIn("user12", "user1");
    }
}
