package com.design.study.designpatterns.behavioral.visitor;

public class ClientC extends Client {
    public ClientC(String name) {
        super(name);
    }

    @Override
    public void run(Visitor visitor) {
        visitor.visit(this);
    }
}
