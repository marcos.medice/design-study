package com.design.study.designpatterns.behavioral.command;

import com.design.study.designpatterns.behavioral.command.commands.*;

import java.util.*;

public class Main {
    public static void main(String args[]){
        Queue<Command> commandQueue = new LinkedList<>();
        // On command for TV with same invoker
        Receiver receiver = new TV();
        commandQueue.add(new OnCommand(receiver));
        commandQueue.add(new ChangeChannelCommand(receiver));
        commandQueue.add(new MuteCommand(receiver));
        commandQueue.add(new OffCommand(receiver));
        do {
            Invoker invoker = new Invoker(commandQueue.poll());
            invoker.execute();
        } while (commandQueue.size() > 0);
        
        // On command for Radio with same invoker
        receiver = new Radio();
        commandQueue.add(new OnCommand(receiver));
        commandQueue.add(new ChangeChannelCommand(receiver));
        commandQueue.add(new MuteCommand(receiver));
        commandQueue.add(new OffCommand(receiver));
        do {
            Invoker invoker = new Invoker(commandQueue.poll());
            invoker.execute();
        } while (commandQueue.size() > 0);
    }
}
