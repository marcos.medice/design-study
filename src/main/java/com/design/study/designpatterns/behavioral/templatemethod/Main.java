package com.design.study.designpatterns.behavioral.templatemethod;

public class Main {
    public static void main(String[] args) {
        ObjectA objectA = new ObjectA();
        ObjectB objectB = new ObjectB();

        objectA.run();
        objectB.run();
    }
}
