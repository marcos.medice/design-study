package com.design.study.designpatterns.behavioral.command.commands;

import com.design.study.designpatterns.behavioral.command.Receiver;

public class OffCommand implements Command {
    private Receiver receiver;

    public OffCommand(Receiver receiver){
        this.receiver = receiver;
    }
    public void execute(){
        receiver.switchOff();
    }
}
