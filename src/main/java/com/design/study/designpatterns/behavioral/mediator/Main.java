package com.design.study.designpatterns.behavioral.mediator;

public class Main {
    public static void main(String[] args) {
        Mediator mediator = new ObjectMediator();
        Object object1 = new ObjectImp("OBJETO 1", mediator);
        Object object2 = new ObjectImp("OBJETO 2", mediator);
        Object object3 = new ObjectImp("OBJETO 3", mediator);
        mediator.addObject(object1);
        mediator.addObject(object2);
        mediator.addObject(object3);

        object1.sendMessage("MENSAGEM");
    }
}
