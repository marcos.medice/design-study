package com.design.study.designpatterns.behavioral.mediator;

public class ObjectImp extends Object {
    public ObjectImp(String name, Mediator mediator) {
        super(name, mediator);
    }

    @Override
    public void sendMessage(String message) {
        System.out.println(super.name + " enviando mensagem: " + message);
        super.mediator.sendMessage(message, this);
    }

    @Override
    public void receiveMessage(String message) {
        System.out.println(super.name + " recebendo mensagem: " + message);
    }
}
