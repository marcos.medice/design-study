package com.design.study.designpatterns.behavioral.visitor;

import java.util.List;

public class VisitorA implements Visitor {
    public void visitorMethod(List<Client> clients) {
        for (Client client : clients) {
            client.run(this);
        }
    }

    @Override
    public void visit(Client client) {
        System.out.println("CLIENT VISITOR: " + client.getName());
    }

//    @Override
//    public void visit(ClientA clientA) {
//        System.out.println("CLIENT A VISITOR: " + clientA.getName());
//    }
//
//    @Override
//    public void visit(ClientB clientB) {
//        System.out.println("CLIENT B VISITOR: " + clientB.getName());
//    }
//
//    @Override
//    public void visit(ClientC clientC) {
//        System.out.println("CLIENT C VISITOR: " + clientC.getName());
//    }
}
