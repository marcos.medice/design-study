package com.design.study.designpatterns.behavioral.observer;

public class CLPEventListener implements EventListener {
    private final String clpId;

    public CLPEventListener(String clpId) {
        this.clpId = clpId;
    }

    @Override
    public void update(Event eventSensor) {
        System.out.println("CLP " + clpId + ": evento recebido -> " + eventSensor);
    }
}
