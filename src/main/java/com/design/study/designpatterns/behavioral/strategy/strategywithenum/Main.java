package com.design.study.designpatterns.behavioral.strategy.strategywithenum;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("METODO 1: ");
        IStrategy<String> strategyA = StrategyEnum.STRATEGY_A.getStrategyClass();
        System.out.println(strategyA.run());
        IStrategy<String> strategyB = StrategyEnum.STRATEGY_B.getStrategyClass();
        System.out.println(strategyB.run());

        System.out.println("-------------------");
        System.out.println("METODO 2: ");
        IStrategy<String> strategyA2 = StrategyEnum.getById("A").getStrategyClass();
        System.out.println(strategyA.run());
        IStrategy<String> strategyB2 = StrategyEnum.getById("B").getStrategyClass();
        System.out.println(strategyB.run());
    }
}
