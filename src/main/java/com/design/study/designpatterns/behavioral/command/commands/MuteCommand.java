package com.design.study.designpatterns.behavioral.command.commands;

import com.design.study.designpatterns.behavioral.command.Receiver;

public class MuteCommand implements Command {
    private Receiver receiver;

    public MuteCommand(Receiver receiver){
        this.receiver = receiver;
    }
    public void execute(){
        receiver.mute();
    }
}
