package com.design.study.designpatterns.behavioral.command.commands;

import com.design.study.designpatterns.behavioral.command.Receiver;

public class OnCommand implements Command {
    private Receiver receiver;

    public OnCommand(Receiver receiver){
        this.receiver = receiver;
    }
    public void execute(){
        receiver.switchOn();
    }
}
