package com.design.study.designpatterns.behavioral.command;

public class Radio implements Receiver {
    public void switchOn(){
        System.out.println("Switch on from Radio");
    }

    @Override
    public void switchOff() {
        System.out.println("Switch off from Radio");
    }

    @Override
    public void changeChannel() {
        System.out.println("Change channel from Radio");
    }

    @Override
    public void mute() {
        System.out.println("Mute from Radio");
    }
}