package com.design.study.designpatterns.behavioral.memento;

public class Main {
    public static void main(String[] args) {
        Editor editor = new Editor();
        editor.write("Lorem");
        System.out.println(editor.getTextArea().getText());
        editor.write("Lorem ipsum");
        System.out.println(editor.getTextArea().getText());
        editor.write("Lorem ipsum velit");
        System.out.println(editor.getTextArea().getText());
        editor.write("Lorem ipsum velit torquent,");
        System.out.println(editor.getTextArea().getText());
        editor.write("Lorem ipsum velit torquent, habitasse");
        System.out.println(editor.getTextArea().getText());

        editor.undo();
        System.out.println(editor.getTextArea().getText());
        editor.undo();
        System.out.println(editor.getTextArea().getText());
        editor.undo();
        System.out.println(editor.getTextArea().getText());
        editor.undo();
        System.out.println(editor.getTextArea().getText());
    }
}
