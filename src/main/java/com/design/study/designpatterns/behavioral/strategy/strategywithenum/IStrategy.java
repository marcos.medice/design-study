package com.design.study.designpatterns.behavioral.strategy.strategywithenum;

public interface IStrategy<R> {

    public R run();
}
