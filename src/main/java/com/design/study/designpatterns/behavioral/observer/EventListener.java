package com.design.study.designpatterns.behavioral.observer;

public interface EventListener {
    void update(Event eventSensor);
}
