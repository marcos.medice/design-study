package com.design.study.designpatterns.behavioral.command;

public class TV implements Receiver {
    public void switchOn(){
        System.out.println("Switch on from TV");
    }

    @Override
    public void switchOff() {
        System.out.println("Switch off from TV");
    }

    @Override
    public void changeChannel() {
        System.out.println("Change channel from TV");
    }

    @Override
    public void mute() {
        System.out.println("Mute from TV");
    }
}
