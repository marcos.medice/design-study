package com.design.study.designpatterns.behavioral.visitor;

public abstract class Client {
    private final String name;

    public Client(String name) {
        this.name = name;
    }

    public abstract void run(Visitor visitor);

    public String getName() {
        return name;
    }
}
