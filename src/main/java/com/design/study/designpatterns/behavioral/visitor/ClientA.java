package com.design.study.designpatterns.behavioral.visitor;

public class ClientA extends Client {
    public ClientA(String name) {
        super(name);
    }

    @Override
    public void run(Visitor visitor) {
        visitor.visit(this);
    }
}
