package com.design.study.designpatterns.behavioral.observer;

public enum Event {
    SENSOR_1,
    SENSOR_2
}
