package com.design.study.designpatterns.behavioral.iterator;

import java.util.Deque;
import java.util.LinkedList;

/*
    Teoria de Grafos
    TODO: NOT IMPLEMENTED
 */
public class BreadthFirstIterator<T> implements Iterator {
    private final Vertex<T> startVertex;
    private final Deque<Vertex<T>> stack = new LinkedList<>();

    public BreadthFirstIterator(Vertex<T> startVertex) {
        this.startVertex = startVertex;
        stack.push(startVertex);
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Vertex<T> getNext() {
        return null;
    }

    @Override
    public void reset() {

    }
}
