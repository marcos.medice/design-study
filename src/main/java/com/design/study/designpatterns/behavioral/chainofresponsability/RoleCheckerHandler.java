package com.design.study.designpatterns.behavioral.chainofresponsability;

public class RoleCheckerHandler extends Handler {
    @Override
    public boolean handle(String username, String password) {
        if ("user1".equals(username)) {
            System.out.println("ACESSO USER1");
            return true;
        }
        System.out.println("ACESSO COMUM");
        return handleNext(username, password);
    }
}
