package com.design.study.designpatterns.behavioral.mediator;

import java.util.LinkedList;
import java.util.List;

public class ObjectMediator implements Mediator {
    private List<Object> objectList;

    public ObjectMediator() {
        this.objectList = new LinkedList<Object>();
    }

    @Override
    public void sendMessage(String message, Object object) {
        for (Object o : objectList) {
            if (o != object) {
                o.receiveMessage(message);
            }
        }
    }

    @Override
    public void addObject(Object object) {
        objectList.add(object);
    }
}
