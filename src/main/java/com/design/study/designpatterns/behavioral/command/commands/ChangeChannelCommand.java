package com.design.study.designpatterns.behavioral.command.commands;

import com.design.study.designpatterns.behavioral.command.Receiver;

public class ChangeChannelCommand implements Command {
    private Receiver receiver;

    public ChangeChannelCommand(Receiver receiver){
        this.receiver = receiver;
    }
    public void execute(){
        receiver.changeChannel();
    }
}
