package com.design.study.designpatterns.behavioral.iterator;

import java.util.List;

public class Vertex<T> {
    private T vertex;
    private List<Vertex<T>> neighbours;
    private boolean visited;

    public Vertex(T vertex) {
        this.vertex = vertex;
        this.visited = false;
    }

    public List<Vertex<T>> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<Vertex<T>> neighbours) {
        this.neighbours = neighbours;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    @Override
    public String toString() {
        return "vertex=" + vertex;
    }
}
