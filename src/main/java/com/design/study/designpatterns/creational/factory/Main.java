package com.design.study.designpatterns.creational.factory;

public class Main {
    public static void main(String[] args) {
        IObject objectA = ObjectFactory.createObject("A");
        if (objectA != null) {
            System.out.println("A: " + objectA.getDescription());
        }

        IObject objectB = ObjectFactory.createObject("B");
        if (objectB != null) {
            System.out.println("B: " + objectB.getDescription());
        }

        IObject objectC = ObjectFactory.createObject("C");
        if (objectC != null) {
            System.out.println("C: " + objectC.getDescription());
        }
    }
}
