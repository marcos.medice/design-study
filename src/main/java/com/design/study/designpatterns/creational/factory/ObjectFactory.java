package com.design.study.designpatterns.creational.factory;

public class ObjectFactory {
    /*
        Ainda viola os princípios de SOLID, aberto a modificacao. Abstrair usando o Factory Method Pattern
     */
    public static IObject createObject(String object) {
        if ("A".equals(object)) {
            return new ObjectA();
        } else if ("B".equals(object)) {
            return new ObjectB();
        } else if ("C".equals(object)) {
            return new ObjectC();
        }
        return null;
    }
}
