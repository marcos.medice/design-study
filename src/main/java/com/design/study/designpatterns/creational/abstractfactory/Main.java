package com.design.study.designpatterns.creational.abstractfactory;

public class Main {
    /*
        Exemplo visual:
            Letras -> Marcas
                A = Razer
                B = Logitech

            Numeros -> Produtos
                One = Mouse
                Two = Teclado
     */
    public static void main(String[] args) {
        ObjectFactory objectAFactory = new ObjectAFactory();
        IObjectOne objectAOne = objectAFactory.createObjectOne();
        IObjectTwo objectATwo = objectAFactory.createObjectTwo();

        ObjectFactory objectBFactory = new ObjectBFactory();
        IObjectOne objectBOne = objectBFactory.createObjectOne();
        IObjectTwo objectBTwo = objectBFactory.createObjectTwo();

        objectAOne.logDescription();
        objectATwo.logDescription();
        objectBOne.logDescription();
        objectBTwo.logDescription();
    }
}
