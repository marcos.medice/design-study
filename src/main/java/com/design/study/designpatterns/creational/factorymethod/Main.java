package com.design.study.designpatterns.creational.factorymethod;

public class Main {
    /*
        Implementacao abstraida, respeitando SOLID
     */
    public static void main(String[] args) {
        ObjectFactory objectAFactory = new ObjectAFactory();
        ObjectFactory objectBFactory = new ObjectBFactory();
        ObjectFactory objectCFactory = new ObjectCFactory();

        IObject objectA = objectAFactory.createObject();
        IObject objectB = objectBFactory.createObject();
        IObject objectC = objectCFactory.createObject();

        System.out.println("A: " + objectA.getDescription());
        System.out.println("B: " + objectB.getDescription());
        System.out.println("C: " + objectC.getDescription());
    }
}
