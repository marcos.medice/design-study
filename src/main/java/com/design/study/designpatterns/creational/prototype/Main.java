package com.design.study.designpatterns.creational.prototype;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ObjectA objectAOne = new ObjectA();
        objectAOne.setCode("abc");

        ObjectA objectATwo = new ObjectA();
        objectATwo.setCode("xyz");

        ObjectB objectBOne = new ObjectB();
        objectBOne.setStatus(true);

        ObjectB objectBTwo = new ObjectB();
        objectBTwo.setStatus(false);

        List<Object> objects = Arrays.asList(objectAOne, objectATwo, objectBOne, objectBTwo);

        List<Object> cloneObjects = new ArrayList<>();

        objects.forEach(object -> {
            cloneObjects.add(object.clone());
        });

        System.out.println("OBJETOS ORIGINAIS: ");
        objects.forEach(System.out::println);

        System.out.println("OBJETOS CLONADOS: ");
        cloneObjects.forEach(System.out::println);
    }
}
