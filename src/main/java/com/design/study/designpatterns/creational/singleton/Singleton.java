package com.design.study.designpatterns.creational.singleton;

public class Singleton {

    private static final Singleton instance = new Singleton();

    public Singleton() {
        // implementacao das configuracoes
    }

    public static Singleton getInstance() {
        return instance;
    }

    public String getInstanceString() {
        return instance.toString();
    }

    public void printTestString() {
        System.out.println("Teste da instancia: " + instance.toString());
    }
}
