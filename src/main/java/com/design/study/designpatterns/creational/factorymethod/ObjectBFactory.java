package com.design.study.designpatterns.creational.factorymethod;

public class ObjectBFactory extends ObjectFactory {
    @Override
    public IObject createObject() {
        return new ObjectB();
    }
}
