package com.design.study.designpatterns.creational.factory;

public class ObjectB implements IObject {
    public String getDescription() {
        return "OBJECT B";
    }
}
