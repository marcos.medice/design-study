package com.design.study.designpatterns.creational.abstractfactory;

public interface IObjectOne {
    public void logDescription();
}
