package com.design.study.designpatterns.creational.factorymethod;

public class ObjectB implements IObject {
    @Override
    public String getDescription() {
        return "OBJECT B";
    }
}
