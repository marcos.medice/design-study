package com.design.study.designpatterns.creational.singleton;

public class Main {

    public static void main(String[] args) {
        System.out.println("Criando instancia");
        String instance = Singleton.getInstance().getInstanceString();
        System.out.println("INSTANCE STRING: " + instance);
        Singleton.getInstance().printTestString();
    }
}
