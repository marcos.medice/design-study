package com.design.study.designpatterns.creational.abstractfactory;

public class ObjectATwo implements IObjectTwo {
    @Override
    public void logDescription() {
        System.out.println("OBJECT A2");
    }
}
