package com.design.study.designpatterns.creational.builder;

public class ObjectBuilder {

    private Long id;
    private String name;
    private String description;
    private Boolean status;

    public ObjectBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public ObjectBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ObjectBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public ObjectBuilder setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Object build() {
        return new Object(id, name, description, status);
    }
}
