package com.design.study.designpatterns.creational.abstractfactory;

public class ObjectAFactory extends ObjectFactory {
    @Override
    public IObjectOne createObjectOne() {
        return new ObjectAOne();
    }
    @Override
    public IObjectTwo createObjectTwo() {
        return new ObjectATwo();
    }
}
