package com.design.study.designpatterns.creational.abstractfactory;

public class ObjectBOne implements IObjectOne {
    @Override
    public void logDescription() {
        System.out.println("OBJECT B1");
    }
}
