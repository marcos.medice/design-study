package com.design.study.designpatterns.creational.builder;

public class Object {

    private final Long id;
    private final String name;
    private final String description;
    private final Boolean status;

    Object(Long id, String name, String description, Boolean status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Object{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                '}';
    }
}
