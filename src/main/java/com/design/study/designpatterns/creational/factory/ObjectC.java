package com.design.study.designpatterns.creational.factory;

public class ObjectC implements IObject {
    public String getDescription() {
        return "OBJECT C";
    }
}
