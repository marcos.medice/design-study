package com.design.study.designpatterns.creational.factory;

public interface IObject {
    public String getDescription();
}
