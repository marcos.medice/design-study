package com.design.study.designpatterns.creational.builder;

public class Main {
    public static void main(String[] args) {
        Object object = new ObjectBuilder()
                .setId(1L)
                .setName("Nome")
                .setDescription("Description")
                .setStatus(true)
                .build();

        Object object2 = new ObjectBuilder()
                .setId(2L)
                .setName("Nome2")
                .setDescription("Description2")
                .setStatus(true)
                .build();

        System.out.println(object);
        System.out.println(object2);

        /*
            Usando Director
         */
        System.out.println("Usando director:");

        ObjectDirector objectDirector = new ObjectDirector();
        ObjectBuilder objectBuilder = new ObjectBuilder();

        objectDirector.buildObjectA(objectBuilder);
        Object objectA = objectBuilder.build();
        System.out.println("Object A: " + objectA);

        objectDirector.buildObjectB(objectBuilder);
        Object objectB = objectBuilder.build();
        System.out.println("Object B: " + objectB);

    }
}
