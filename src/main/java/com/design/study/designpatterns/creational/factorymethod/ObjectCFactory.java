package com.design.study.designpatterns.creational.factorymethod;

public class ObjectCFactory extends ObjectFactory {
    @Override
    public IObject createObject() {
        return new ObjectC();
    }
}
