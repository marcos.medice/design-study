package com.design.study.designpatterns.creational.abstractfactory;

public abstract class ObjectFactory {
    public abstract IObjectOne createObjectOne();
    public abstract IObjectTwo createObjectTwo();
}
