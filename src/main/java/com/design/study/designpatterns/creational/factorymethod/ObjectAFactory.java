package com.design.study.designpatterns.creational.factorymethod;

public class ObjectAFactory extends ObjectFactory {
    @Override
    public IObject createObject() {
        return new ObjectA();
    }
}
