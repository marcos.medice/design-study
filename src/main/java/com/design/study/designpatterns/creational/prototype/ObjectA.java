package com.design.study.designpatterns.creational.prototype;

public class ObjectA extends Object {
    private String code;

    public ObjectA() {};

    public ObjectA(ObjectA objectA) {
        super(objectA);
        this.code = objectA.code;
    }

    @Override
    public ObjectA clone() {
        return new ObjectA(this);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ObjectA{" +
                "code='" + code + '\'' +
                '}';
    }
}
