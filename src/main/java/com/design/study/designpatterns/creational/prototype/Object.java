package com.design.study.designpatterns.creational.prototype;

public abstract class Object {
    private String name;
    private String description;

    protected Object(Object object) {
        this.name = object.name;
        this.description = object.description;
    }

    public Object() {

    }

    public abstract Object clone();

    @Override
    public String toString() {
        return "Object{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
