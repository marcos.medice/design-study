package com.design.study.designpatterns.creational.abstractfactory;

public class ObjectBFactory extends ObjectFactory {
    @Override
    public IObjectOne createObjectOne() {
        return new ObjectBOne();
    }
    @Override
    public IObjectTwo createObjectTwo() {
        return new ObjectBTwo();
    }
}
