package com.design.study.designpatterns.creational.factorymethod;

public class ObjectC implements IObject {
    @Override
    public String getDescription() {
        return "OBJECT C";
    }
}
