package com.design.study.designpatterns.creational.abstractfactory;

public class ObjectBTwo implements IObjectTwo {
    @Override
    public void logDescription() {
        System.out.println("OBJECT B2");
    }
}
