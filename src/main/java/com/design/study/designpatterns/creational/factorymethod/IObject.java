package com.design.study.designpatterns.creational.factorymethod;

public interface IObject {
    public String getDescription();
}
