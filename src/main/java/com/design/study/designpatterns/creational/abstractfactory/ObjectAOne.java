package com.design.study.designpatterns.creational.abstractfactory;

public class ObjectAOne implements IObjectOne {
    @Override
    public void logDescription() {
        System.out.println("OBJECT A1");
    }
}
