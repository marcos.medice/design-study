package com.design.study.designpatterns.creational.factorymethod;

public abstract class ObjectFactory {
    public abstract IObject createObject();
}
