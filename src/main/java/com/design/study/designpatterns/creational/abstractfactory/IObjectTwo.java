package com.design.study.designpatterns.creational.abstractfactory;

public interface IObjectTwo {
    public void logDescription();
}
