package com.design.study.designpatterns.creational.builder;

public class ObjectDirector {
    public void buildObjectA(ObjectBuilder builder) {
        builder.setName("Object A")
                .setDescription("A")
                .setStatus(true);
    }

    public void buildObjectB(ObjectBuilder builder) {
        builder.setName("Object B")
                .setDescription("B")
                .setStatus(true);
    }
}
