package com.design.study.designpatterns.creational.prototype;

public class ObjectB extends Object {
    private Boolean status;

    public ObjectB() {};

    public ObjectB(ObjectB objectA) {
        super(objectA);
        this.status = objectA.status;
    }

    @Override
    public ObjectB clone() {
        return new ObjectB(this);
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ObjectB{" +
                "status=" + status +
                '}';
    }
}
