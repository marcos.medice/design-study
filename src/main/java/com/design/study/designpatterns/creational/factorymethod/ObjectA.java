package com.design.study.designpatterns.creational.factorymethod;

public class ObjectA implements IObject {
    @Override
    public String getDescription() {
        return "OBJECT A";
    }
}
