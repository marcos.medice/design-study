package com.design.study.designpatterns.creational.factory;

public class ObjectA implements IObject {

    public String getDescription() {
        return "OBJECT A";
    }
}
